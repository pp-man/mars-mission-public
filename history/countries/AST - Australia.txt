﻿capital = 1

set_politics = {
	ruling_party = conservative
	last_election = "2200.11.8"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	anarchist = 0
	collectivist = 8
	social_democrat = 22
	liberal = 22
	centrism = 0
	conservative = 34
	authoritarian_democrat = 8
	paternal_autocrat = 0
	national_populist = 6
	monarchist = 0
}

create_country_leader = {  #These are just some WIP test examples of leaders done for australia since it is most complete
	name = "Delilah Carey"
	desc = "POLITICS_KURT_SCHUSCHNIGG_DESC"
	picture = "portrait_australia_delilah.dds"
	expire = "2210.1.1"
	ideology = social_democracy
	traits = {
		devout_scientist
	}
}

create_country_leader = {
	name = "Brutus Joner"
	desc = "POLITICS_KURT_SCHUSCHNIGG_DESC"
	picture = "portrait_australia_brutus.dds"
	expire = "2210.1.1"
	ideology = social_liberal
	traits = {
		martian_born
	}
}

create_country_leader = {
	name = "S3-M7"
	desc = "POLITICS_KURT_SCHUSCHNIGG_DESC"
	picture = "portrait_australia_robot.dds"
	expire = "2210.1.1"
	ideology = military_junta
	traits = {
		dictator
	}
}

create_country_leader = {
	name = "Dave Fluttercock"
	desc = "POLITICS_KURT_SCHUSCHNIGG_DESC"
	picture = "portrait_australia_dave.dds"
	expire = "2210.1.1"
	ideology = fiscal_conservative
	traits = {
		autocratic_imperialist
		conservative_grandee
	}
}