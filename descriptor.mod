version="1.8.*"
tags={
	"Alternative History"
	"Gameplay"
}
name="marsmission"
supported_version="1.8.1"

replace_path = "common/ai_strategy"

replace_path = "history/countries"
replace_path = "history/states"
replace_path = "history/units"

replace_path = "map/strategicregions"
replace_path = "map/supplyareas"
replace_path = "map"
